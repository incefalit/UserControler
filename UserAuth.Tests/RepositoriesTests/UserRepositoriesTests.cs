﻿namespace UserAuth.Tests.RepositoriesTests
{
    using Microsoft.EntityFrameworkCore;

    using System;

    using User.Data;
    using User.Data.Entities;
    using User.Data.Repositories.Implementations;

    using Xunit;

    public class UserRepositoriesTests //Added some unit tests for repo
    {
        [Fact]
        public async void GetAllUsersAsync_WithoutSearch_ShouldReturnAllUsers()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            await using (var context = new DataContext(options))
            {
                context.Users.AddRange(
                    new User { Name = "User1", Email = "ccc@ccc" },
                    new User { Name = "User2", Email = "aaa@aaa" },
                    new User { Name = "User3", Email = "BBB@BBB" }
                );
                await context.SaveChangesAsync();
            }

            await using (var context = new DataContext(options))
            {
                var userRepository = new UserRepository(context);
                var searchByUserName = string.Empty;

                // Act
                var result = await userRepository.GetAllUsersAsync(searchByUserName);

                // Assert
                Assert.Equal(3, result.Count);
            }
        }

        [Fact]
        public async void GetAllUsersAsync_WithSearch_ShouldReturnFilteredUsers()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            await using (var context = new DataContext(options))
            {
                context.Users.AddRange(
                    new User { Name = "User1", Email = "ccc@ccc" },
                    new User { Name = "User2", Email = "aaa@aaa" },
                    new User { Name = "User3", Email = "BBB@BBB" }
                );
                await context.SaveChangesAsync();
            }

            await using (var context = new DataContext(options))
            {
                var userRepository = new UserRepository(context);
                var searchByUserName = "User2";

                // Act
                var result = await userRepository.GetAllUsersAsync(searchByUserName);

                // Assert
                Assert.Single(result);
                Assert.Equal("User2", result[0].Name);
            }
        }

        [Fact]
        public async Task GetUserByIdAsync_ExistingUser_ShouldReturnUser()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            await using (var context = new DataContext(options))
            {
                var user = new User
                {
                    Id = 1,
                    Name = "TestUser",
                    Email = "AAA@AAA",
                    Age = 16
                };

                context.Users.Add(user);
                await context.SaveChangesAsync();
            }

            await using (var context = new DataContext(options))
            {
                var userRepository = new UserRepository(context);

                // Act
                var result = await userRepository.GetUserByIdAsync(1);

                // Assert
                Assert.NotNull(result);
                Assert.Equal("TestUser", result.Name);
            }
        }

        [Fact]
        public async Task GetUserByIdAsync_NonExistingUser_ShouldReturnNull()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            await using var context = new DataContext(options);
            var userRepository = new UserRepository(context);

            // Act
            var result = await userRepository.GetUserByIdAsync(1);

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task AddNewRoleAsync_RoleNotExists_ShouldAddRole()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            await using var context = new DataContext(options);
            var userRepository = new UserRepository(context);

            var role = new Role
            {
                Id = 1,
                Name = "NewRole"
            };

            // Act
            await userRepository.AddNewRoleAsync(role);

            // Assert
            var addedRole = await context.Roles.FirstOrDefaultAsync(r => r.Name == "NewRole");
            Assert.NotNull(addedRole);
            Assert.Equal("NewRole", addedRole.Name);
        }

        [Fact]
        public async Task AddNewRoleAsync_RoleExists_ShouldThrowException()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            await using var context = new DataContext(options);
            var userRepository = new UserRepository(context);

            var role = new Role
            {
                Id = 1,
                Name = "ExistingRole"
            };

            context.Roles.Add(role);
            await context.SaveChangesAsync();

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => userRepository.AddNewRoleAsync(role));
        }

        [Fact]
        public async Task RoleAddToUserAsync_UserAndRoleExist_ShouldAddUserRole()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            await using var context = new DataContext(options);
            var userRepository = new UserRepository(context);

            var user = new User
            {
                Id = 1,
                Name = "TestUser",
                Email = "AAA@AAA",
                Age = 16
            };

            var role = new Role
            {
                Id = 1,
                Name = "Role1"
            };

            context.Users.Add(user);
            context.Roles.Add(role);
            await context.SaveChangesAsync();

            // Act
            await userRepository.RoleAddToUserAsync(user.Id, role.Id);

            // Assert
            var userRole = await context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == user.Id && ur.RoleId == role.Id);
            Assert.NotNull(userRole);
            Assert.Equal(user.Id, userRole.UserId);
            Assert.Equal(role.Id, userRole.RoleId);
        }

        [Fact]
        public async Task RoleAddToUserAsync_UserOrRoleNotExist_ShouldThrowException()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            await using var context = new DataContext(options);
            var userRepository = new UserRepository(context);

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => userRepository.RoleAddToUserAsync(1, 1));
        }

        [Fact]
        public async Task RoleAddToUserAsync_UserRoleExists_ShouldThrowException()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            await using var context = new DataContext(options);
            var userRepository = new UserRepository(context);

            var user = new User
            {
                Id = 1,
                Name = "TestUser",
                Email = "AAA@AAA",
                Age = 16
            };

            var role = new Role
            {
                Id = 1,
                Name = "Role1"
            };

            var userRole = new UserRoles
            {
                UserId = user.Id,
                RoleId = role.Id
            };

            context.Users.Add(user);
            context.Roles.Add(role);
            context.UserRoles.Add(userRole);
            await context.SaveChangesAsync();

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => userRepository.RoleAddToUserAsync(user.Id, role.Id));
        }

        [Fact]
        public async Task RegisterAsync_UserAlreadyExists_ShouldThrowException()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            await using var context = new DataContext(options);
            var userRepository = new UserRepository(context);

            var existingUser = new User
            {
                Name = "ExistingUser",
                Age = 30,
                Email = "existinguser@example.com"
            };

            context.Users.Add(existingUser);
            await context.SaveChangesAsync();

            var newUser = new User
            {
                Name = "NewUser",
                Age = 25,
                Email = "existinguser@example.com"
            };

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => userRepository.RegisterAsync(newUser));
        }
    }
}

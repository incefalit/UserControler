﻿namespace User.Controllers
{
    using AutoMapper;

    using Microsoft.AspNetCore.Mvc;
    using Microsoft.IdentityModel.Tokens;

    using Serilog;
    
    using User.Application.Models;
    using User.Application.Services.Interfaces;
    using User.Data.Entities;

    public class UserController : ControllerBase
    {
        private readonly IUserService _userServices;

        private readonly IMapper _autoMapper;

        public UserController(IUserService userService, IMapper autoMapper)
        {
            _userServices = userService;
            _autoMapper = autoMapper;
        }

        [HttpGet("GetAllUsers")]
        public async Task<IActionResult> GetAllUsers(
            string searchByUserName = null,
            string sortUserBy = "Id",
            string sortRoleBy = "Id",
            int pageSize = 10,
            int pageNumber = 1)
        {
            try
            {
                if (pageSize <= 0 || pageNumber <= 0)
                {
                    return BadRequest("Invalid pageSize or pageNumber");
                }

                var users = await _userServices.GetAllUsersAsync(searchByUserName, sortUserBy, sortRoleBy);
                
                var paginatedUsers = users
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();

                if (!paginatedUsers.IsNullOrEmpty())
                {
                    return Ok(paginatedUsers);
                }

                Log.Error("Users not found");
                return NotFound("Users not found");
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return BadRequest("An error occurred while processing your request.");
            }
        }

        [HttpDelete("DeleteUserById")]
        public async Task<IActionResult> DeleteUserById(int userId)
        {
            try
            {
                await _userServices.DeleteUserById(userId);
                Log.Information("User with ID {0} was deleted", userId);
                return Ok("User was deleted");
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return BadRequest("An error occurred while processing your request.");
            }
        }

        [HttpGet("GetUserById")]
        public async Task<IActionResult> GetUserById(int userId)
        {
            try
            {
                var user = await _userServices.GetUserByIdAsync(userId);
                return Ok(user);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return BadRequest("An error occurred while processing your request.");
            }
        }

        [HttpPost("CreateNewRole")]
        public async Task<IActionResult> CreateNewRole([FromBody] RoleRegisterModel roleRegisterModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    Log.Error("Validation is not passed");
                    return BadRequest("Check the correctness of the entered fields");
                }

                var role = _autoMapper.Map<Role>(roleRegisterModel);

                try
                {
                    await _userServices.AddNewRoleAsync(role);
                    return Ok("Role registered successfully");
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    return BadRequest("Role with the same name already exists.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return BadRequest("An error occurred while processing your request.");
            }
        }

        [HttpPost("CreateNewUser")]
        public async Task<IActionResult> CreateNewUser([FromBody] UserRegisterModel userRegisterModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    Log.Error("Validation is not passed");
                    return BadRequest("Check the correctness of the entered fields");
                }

                var user = _autoMapper.Map<User>(userRegisterModel);

                try
                {
                    await _userServices.RegisterAsync(user);
                    return Ok("User registered successfully");
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    return BadRequest("User with the same email already exists.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return BadRequest("An error occurred while processing your request.");
            }
        }

        [HttpPost("AddRoleToUser")]
        public async Task<IActionResult> AddRoleToUser(int userId, int roleId)
        {
            try
            {
                await _userServices.RoleAddToUserAsync(userId, roleId);
                return Ok("Role added to user successfully");
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return BadRequest("An error occurred while processing your request.");
            }
        }

        [HttpPut("UpdateUser/{userId}")]
        public async Task<IActionResult> UpdateUser([FromRoute] int userId, [FromBody] UserRegisterModel updatedUserData)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    Log.Error("Validation is not passed");
                    return BadRequest("Check the correctness of the entered fields");
                }

                var updatedUser = _autoMapper.Map<User>(updatedUserData);
                await _userServices.UpdateUserAsync(userId, updatedUser);

                return Ok("User updated successfully");
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return BadRequest("An error occurred while processing your request.");
            }
        }
    }
}

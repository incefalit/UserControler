﻿namespace User.Data.Repositories.Interfaces
{
    using User.Data.Entities;

    public interface IUserRepository
    {
        Task<List<User>> GetAllUsersAsync(string searchByUserName);

        Task<User> GetUserByIdAsync(int userId);

        Task AddNewRoleAsync(Role role);

        Task RoleAddToUserAsync(int userId, int roleId);

        Task RegisterAsync(User user);

        Task UpdateUserAsync(int userId, User user);

        Task DeleteUserByIdAsync(int userId);

        Task SaveChangesAsync();
    }
}

﻿namespace User.Data.Repositories.Implementations
{
    using Microsoft.EntityFrameworkCore;

    using Serilog;

    using User.Data.Entities;
    using User.Data.Repositories.Interfaces;

    public class UserRepository : IUserRepository
    {
        private readonly DataContext _context;

        public UserRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<List<User>> GetAllUsersAsync(string searchByUserName)
        {
            try
            {
                if (string.IsNullOrEmpty(searchByUserName))
                {
                    var users = await _context.Users
                        .Include(ur => ur.UserRoles)
                        .ThenInclude(r => r.Role)
                        .ToListAsync();
                    return users;
                }

                var user = await _context.Users
                    .Where(n => n.Name.ToLower() == searchByUserName.ToLower())
                    .ToListAsync();
                return user;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw new Exception("Users not found");
            }
        }

        public async Task<User> GetUserByIdAsync(int userId)
        {
            try
            {
                var user =  await _context.Users
                    .Include(ur => ur.UserRoles)
                    .ThenInclude(r => r.Role)
                    .FirstOrDefaultAsync(i => i.Id == userId);
                return user;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw new Exception("Users not found");
            }
        }

        public async Task AddNewRoleAsync(Role role)
        {
            var existingRole = await _context.Roles
                .FirstOrDefaultAsync(r => r.Name == role.Name);

            if (existingRole != null)
            {
                Log.Error("Role with the same name already exists");
                throw new Exception("Role with the same name already exists");
            }

            _context.Roles.Add(role);
            await SaveChangesAsync();
        }

        public async Task RoleAddToUserAsync(int userId, int roleId)
        {
            var user = await _context.Users
                .FirstOrDefaultAsync(u => u.Id == userId);
            var role = await _context.Roles
                .FirstOrDefaultAsync(r => r.Id == roleId);

            if (user == null || role == null)
            {
                Log.Error("User or role not found");
                throw new Exception("User or role not found");
            }

            var existingUserRole = await _context.UserRoles
                .FirstOrDefaultAsync(ur => ur.UserId == userId && ur.RoleId == roleId);

            if (existingUserRole != null)
            {
                Log.Error("User already has this role");
                throw new Exception("User already has this role");
            }

            var userRole = new UserRoles
            {
                UserId = userId,
                RoleId = roleId
            };

            _context.UserRoles.Add(userRole);
            await SaveChangesAsync();
        }

        public async Task RegisterAsync(User user)
        {
            var existingUser = await _context.Users
                .FirstOrDefaultAsync(u => u.Email == user.Email);

            if (existingUser != null)
            {
                Log.Error("User with the same email already exists");
                throw new Exception("User with the same email already exists");
            }

            _context.Users.Add(user);
            await SaveChangesAsync();
        }

        public async Task UpdateUserAsync(int userId, User updatedUser)
        {
            var existingUser = await _context.Users
                .FirstOrDefaultAsync(u => u.Id == userId);
            if (existingUser == null)
            {
                Log.Error("User not found");
                throw new Exception("User not found");
            }

            existingUser.Name = updatedUser.Name;
            existingUser.Age = updatedUser.Age;
            existingUser.Email = updatedUser.Email;

            _context.Users.Update(existingUser);
            await SaveChangesAsync();
        }

        public async Task DeleteUserByIdAsync(int userId)
        {
            try
            {
                var user = await _context.Users
                    .FirstOrDefaultAsync(u => u.Id == userId);

                if (user != null)
                {
                    _context.Users.Remove(user);
                    await SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw new Exception("User not found");
            }
        }

        public async Task SaveChangesAsync()
        {
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
    }
}

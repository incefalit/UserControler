﻿namespace User.Data.Entities
{
    using System.ComponentModel.DataAnnotations;

    public class Role
    {
        public int Id { get; set; }

        [Required]
        [StringLength(35)]
        public string Name { get; set; }

        public ICollection<UserRoles> UserRoles { get; set; }
    }
}

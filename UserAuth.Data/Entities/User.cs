﻿namespace User.Data.Entities
{
    using System.ComponentModel.DataAnnotations;

    public class User
    {
        public int Id { get; set; }

        [Required]
        [StringLength(35)]
        public string Name { get; set; }

        [Required]
        [Range(16, 120)]
        public int Age { get; set; }

        [Required]
        [StringLength(35)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        public ICollection<UserRoles> UserRoles { get; set; }
    }
}

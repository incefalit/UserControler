﻿namespace User.Application.Models
{
    using System.ComponentModel.DataAnnotations;

    public class UserRegisterModel
    {
        [Required]
        [StringLength(35)]
        public string Name { get; set; }

        [Required]
        [Range(1, 120)]
        public int Age { get; set; }

        [Required]
        [StringLength(35)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }
    }
}

﻿namespace User.Application.Models
{
    using System.ComponentModel.DataAnnotations;

    public class RoleRegisterModel
    {
        [Required]
        [StringLength(35)]
        public string Name { get; set; }
    }
}

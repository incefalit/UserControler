﻿namespace User.Application.DTO
{
    public class UserResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }

        public string Email { get; set; }

        public List<RoleResponse> Roles { get; set; }
    }
}

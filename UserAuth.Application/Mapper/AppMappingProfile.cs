﻿namespace User.Application.Mapper
{
    using AutoMapper;

    using User.Application.DTO;
    using User.Application.Models;
    using User.Data.Entities;

    public class AppMappingProfile : Profile
    {
        public AppMappingProfile()
        {
            CreateMap<UserRegisterModel, User>();
            CreateMap<RoleRegisterModel, Role>();
            CreateMap<User, UserResponse>()
                .ForMember(x => x.Roles, s => s.MapFrom(x => x.UserRoles.Select(x => x.Role)));
            CreateMap<Role, RoleResponse>()
                .ForMember(x => x.Name, s => s.MapFrom(x => x.UserRoles.FirstOrDefault().Role.Name))
                .ForMember(x => x.Id, s => s.MapFrom(x => x.UserRoles.FirstOrDefault().Role.Id));
        }
    }
}

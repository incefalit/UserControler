﻿namespace User.Application.Services.Implementations
{
    using AutoMapper;

    using Serilog;

    using User.Application.DTO;
    using User.Application.Services.Interfaces;
    using User.Data.Entities;
    using User.Data.Repositories.Interfaces;

    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<List<UserResponse>> GetAllUsersAsync(string searchByUserName, string sortUserBy, string sortRoleBy)
        {
            var users = await _userRepository.GetAllUsersAsync(searchByUserName);
            var userDto = users
                .Select(u => _mapper.Map<UserResponse>(u));

            if (sortRoleBy.ToLower() == "name")
            {
                try
                {
                    userDto = userDto.Select(x =>
                    {
                        x.Roles = x.Roles
                            .OrderBy(r => r.Name)
                            .ToList();
                        return x;
                    });
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    throw new Exception("User has no roles");
                }
            }

            return sortUserBy.ToLower() switch
            {
                "name" => userDto
                    .OrderBy(x => x.Name)
                    .ToList(),
                "id" => userDto
                    .OrderBy(x => x.Id)
                    .ToList(),
                "email" => userDto
                    .OrderBy(x => x.Email)
                    .ToList(),
                _ => userDto
                    .ToList()
            };
        }

        public async Task RegisterAsync(User user)
        {
            await _userRepository.RegisterAsync(user);
        }

        public async Task AddNewRoleAsync(Role role)
        {
            await _userRepository.AddNewRoleAsync(role);
        }

        public async Task RoleAddToUserAsync(int userId, int roleId)
        {
            await _userRepository.RoleAddToUserAsync(userId, roleId);
        }

        public async Task DeleteUserById(int userId)
        {
            await _userRepository.DeleteUserByIdAsync(userId);
        }

        public async Task<UserResponse> GetUserByIdAsync(int userId)
        {
            var user = await _userRepository.GetUserByIdAsync(userId);
            return _mapper.Map<UserResponse>(user);
        }

        public async Task UpdateUserAsync(int userId, Data.Entities.User updatedUser)
        {
            await _userRepository.UpdateUserAsync(userId, updatedUser);
        }
    }
}

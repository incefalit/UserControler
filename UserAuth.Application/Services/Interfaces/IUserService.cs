﻿namespace User.Application.Services.Interfaces
{
    using User.Application.DTO;
    using User.Data.Entities;

    public interface IUserService
    {
        Task<List<UserResponse>> GetAllUsersAsync(string searchByUserName, string sortUserBy, string sortRoleBy);

        Task RegisterAsync(User user);

        Task AddNewRoleAsync(Role role);

        Task RoleAddToUserAsync(int userId, int roleId);

        Task DeleteUserById(int userId);

        Task<UserResponse> GetUserByIdAsync(int userId);

        Task UpdateUserAsync(int userId, User updatedUser);
    }
}
